const pg = require('pg');

async function checkLegalUser(email, client) {

    const query = {
        // give the query a unique name
        name: 'fetch-user',
        text: 'select * from "CompanyEmails" where email = $1',
        values: [email],
    }

    const result = await client.query(query);
    const companyEmail = result.rows[0];
    if (companyEmail) return 'LEGAL'

    return 'NORMAL'

}

exports.lambdaHandler = async (event, context) => {

    console.log(JSON.stringify(event));

    try {
        const attributes = event.request['userAttributes'];
        let userProvider = "COGNITO";
        console.log(attributes["cognito:user_status"])
        if (attributes["cognito:user_status"] === "EXTERNAL_PROVIDER") {
            userProvider = event['userName'].split('_')[0].toUpperCase();
        }
        const client = new pg.Client(process.env.leadership_db_connection_string);
        client.connect();

        const userType = await checkLegalUser(attributes.email, client)

        const queryString = `Insert into "User"("email", "cognitoId", "name", "surname", "provider", "type")
                             VALUES ('${attributes.email}', '${event['userName']}', '${attributes.name}',
                                     '${attributes.family_name}', '${userProvider}', '${userType}');`;
        await client.query(queryString);
        return null, event;
    } catch (err) {
        console.log(err);
        //todo send email
        return null, event;
    }

};

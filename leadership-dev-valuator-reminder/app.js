const axios = require('axios');
/**
 *
 Calls valuator reminder api
 *
 */
exports.lambdaHandler = async (event, context) => {

    console.log("event", JSON.stringify(event));
    try {
        const server_base_url = process.env.server_url;
        const result = await axios.post(server_base_url + '/valuator/remind-notifications');
        console.log("result", result)
        return null, event;
    } catch (e) {
        console.log("e", e);
        return null, event;
    }

};
// exports.lambdaHandler().then();
